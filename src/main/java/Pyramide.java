import java.util.ArrayList;
import java.util.List;

public class Pyramide {
    public List<String> pyramide(Integer taille) throws ArrayIndexOutOfBoundsException {
        if(taille <= 0) throw new ArrayIndexOutOfBoundsException();

        List<String> retour = new ArrayList<>();
        for (int i = 0; i < taille; i++)
            retour.add("*".repeat(i+1));

        for (int i = taille-1; i > 0; i--)
            retour.add("*".repeat(i));

        return retour;
    }

    public static void main(String[] args) {
        Pyramide pyramide = new Pyramide();
        pyramide.pyramide(3).forEach(System.out::println);
    }
}
