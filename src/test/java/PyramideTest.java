import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PyramideTest {

    static Pyramide pyramide;

    @BeforeAll
    static void init() {
        pyramide = new Pyramide();
    }

    @DisplayName("La liste doit être une liste de String non vide")
    @ParameterizedTest
    @ValueSource(ints = {1,2,7,23})
    void testPyramide(int taille) {
        List<String> result = pyramide.pyramide(taille);
        assertNotNull(result);
        for (String s : result)
            assertNotNull(s);
    }

    @DisplayName("Elle doit retourner une liste de longueur (taille)")
    @ParameterizedTest
    @ValueSource(ints = {1,2,5,45})
    void testBonneLongueur(int taille) {
        List<String> result = pyramide.pyramide(taille);
        assertEquals(taille*2-1,result.size());
    }

    @DisplayName("Elle doit retourner une exception si la taille est inférieur ou égale a 0")
    @Test
    void testTailleZero() {
        Integer taille = 0;
        assertThrows(ArrayIndexOutOfBoundsException.class,() -> pyramide.pyramide(taille));
    }

    @DisplayName("Les String sont composé d'*")
    @ParameterizedTest
    @ValueSource(ints = {1,2,5,45})
    void testIsOnlyStar(int taille) {
        List<String> result = pyramide.pyramide(taille);
        result.forEach((s) -> assertTrue(s.matches("^\\*+$")));
    }

}